package main;

public class Stack_2 {
	String st[]=new String[10];
	int top = -1;
	int cap = 5;
	
	void push(String item){
		if(top==5){
			System.out.println("^^^^~~~~****...Stack overflow...****~~~~^^^^" +top +":"+ cap);
		}else st[++top]=item;
	}
	
	String pop(){
		if(top<0){
			return "...Stack Undeflow...";
		}else return st[top--];
	}
	
	public static void main(String args[]){
		Stack_2 s=new Stack_2();
		
		s.push("pur");
		s.push("awesome");
		s.push("gr8");
		s.push("stack");
		s.push("flow");
		s.push("fish");
		s.push("azazel");
		
		System.out.println("\n\n\n"+s.pop());
		System.out.println(s.pop());
		System.out.println(s.pop());
		System.out.println(s.pop());
		System.out.println(s.pop());
		System.out.println(s.pop());
		System.out.println(s.pop());
	}
	
}
