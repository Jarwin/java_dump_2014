package main;

import java.io.DataInputStream;
import java.io.IOException;

public class Stack {
	static int stck[]=new int [200];
	static int capacity;
	int tos;
	
	Stack(){  //Default constructor for initializing array to zero
	for(int i = 0; i<200; i++){
			stck[i]=0;
		}
	}
	
	Stack(int cap){ //parameterized constructor to initialize `tos` and `capacity` 
		capacity = cap;
		tos= -1;
	}
	
	void push(int item){ // To push element at the top of the stack
		if(tos==capacity){
			System.out.println("Stack overflow, The elements cannot be pushed");
		}else{
			
		    stck[++tos]=item;
		}
	}

	int pop(){ // To take elements out from the top of the stack
		if(tos<0){
			return -9999;
		}else{ return stck[tos--];	}
	}

	void print_stack(){
		if(pop()==-9999){
			System.out.println("Underflow...");
		}else
		System.out.println(pop());
	}

	public static void main(String args[])throws IOException{ // Main method
		DataInputStream d=new DataInputStream(System.in);
		boolean bool=true; //Value of capacity
		do{
			System.out.print("Value of capacity: ");
			int cap = Integer.parseInt(d.readLine());
			if(cap<=200){ bool=true; break;}  
			else{ System.out.println("Out of bound, Try again"); }
		}while(bool);
		
		//OBJECT_FOR_STACK_CLASS
		Stack stt = new Stack(10);
		
		//Menu
	
		
		boolean bool2=true;
		do{
			
			System.out.println("Avilable Options:");
			System.out.println("1. Push");
			System.out.println("2. Pop");
			System.out.println("3. Show Stack");
			System.out.println("4. Quit");
			System.out.print("Your Choice: ");
			int choice= Integer.parseInt(d.readLine());
			
			switch(choice){
			case 1:
					int val = Integer.parseInt(d.readLine());
					stt.push(val);
					System.out.println("Value Pushed = "+ val);
					break;
			case 2:
					System.out.println(stt.pop());
					break;
			case 3:
					stt.print_stack();
					break;
			case 4:	
					bool2=false;
					break;
			default:
				System.out.println("Invalid option detected! Try again");
				break;
			}
		}while(bool2);
	
	}
}
