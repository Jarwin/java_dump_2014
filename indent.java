import java.io.*;
import java.util.StringTokenizer;

public class indent {
    public static void main(String [] args) {

        // The name of the file to open.
        String fileName = "alice.1txtt";

        // This will reference one line at a time
        String line = null;

        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = 
                new FileReader(fileName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);
            StringTokenizer st;
            DataInputStream d=new DataInputStream(System.in);
            System.out.println("Enter the word to be searched: ");
            String word= d.readLine();
            int counter = 0;
            while((line = bufferedReader.readLine()) != null) {
            	 st = new StringTokenizer(line);
            	 if (st.nextToken()==word){
            		 counter ++;
            	 }
            	
            }	
            System.out.println(counter+" number of instances of '"+word+"' found");
            // Always close files.
            bufferedReader.close();			
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");				
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");					
            // Or we could just do this: 
            // ex.printStackTrace();
        }
    }
}