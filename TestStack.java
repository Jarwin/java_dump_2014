package main;

import java.util.*;

public class TestStack {
				static int tos = -1;
				int capacity= 5;
				String stck[]=new String[capacity];    

        
    	String push(String item){
    		if(tos==capacity){
    			System.out.println("Stack overflow, The elements cannot be pushed");
    		}else{ stck[++tos]=item; }
    		return "";
   		}
    	
    	String pop(){
    		if(tos<0){
    			return null;
    		}else{ return stck[tos--];	}
    	}
    	
    	  static void printStack() {
    	        if(tos<0) {
    	            System.out.println("You have nothing in your stack");
    	        } else {
    	            System.out.printf(" TOP \n" );
    	        }
    	    }
    	
    	public static void main(String[] args) {
    		TestStack stack=new TestStack();
    		stack.push("bottom");
    		printStack();
    		stack.push("second");
    		printStack();
    		stack.push("third");
    		printStack();

    		stack.pop();
    		printStack();
    		stack.pop();
    		printStack();
    		stack.pop();
    		printStack();

    }

    //printStack methid
  
}