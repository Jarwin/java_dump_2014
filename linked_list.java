public class Node{
  private String data;
  Node link;

  Node(String data, Node link){
    this.data = data;
    this.link = link;
  }

  //Getters and setters
  public String getData(){return data;}
  public Node getNext(){return link;}
  public void setData(String d){data=d;}
  public void setNext(Node n){link=n;}
}

class set_node{
  Node cNode = new Node("I am C", null);
  Node bNode = new Node("I am B", cNode);
  Node aNode = new Node("I am A", bNode);
  Node first = new Node("First", aNode);

  void display(){
    System.out.println(cNode.getData() +" : " + cNode.getNext());
    System.out.println(aNode.getData() +" : " + aNode.getNext());
    System.out.println(bNode.getData() +" : " + bNode.getNext());
    System.out.println(first.getData() +" : " + first.getNext());
    /*
    first.setData("I have been altered");
    first.setNext(bNode);
    System.out.println();
    System.out.println(first.getData()+" : "+first.getNext());
    */
  }

  public static String getThird(Node list){
    return list.getNext().getNext().getData();
  }

  public static void insertSecond(Node list, String s){
    Node newNode = new Node(s, null);
    newNode.setNext(list.getNext());
    list.setNext(newNode);
  }

  public int size(Node list){
    int count = 0;
    while (list.getNext() != null){
      count++;
      list = list.getNext();
    }
    return count;
  }

  public static void main(String args[]){
      set_node nn = new set_node();
      nn.display();
      nn.size(list);
  }
  
}
