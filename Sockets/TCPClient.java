import java.io.*;
import java.net.*;

public class TCPClient {
	public static void main(String args[])throws IOException{
		String sentence;
		String modefiedSentence;
		
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		Socket clientSocket = new Socket("127.0.0.1", 10085);
		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		sentence = inFromUser.readLine();
		outToServer.writeBytes(sentence + '\n');
		modefiedSentence = inFromServer.readLine();
		System.out.println("FROM SERVER: " + modefiedSentence);
		clientSocket.close();
		
		
	}
}
